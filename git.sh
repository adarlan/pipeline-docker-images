#!/bin/bash
set -e

git clone "${CI_REPOSITORY_URL}" /clone
cd /clone

if [ "${CI_COMMIT_BRANCH}" != "" ]; then
    MainBranch="$(git rev-parse --abbrev-ref HEAD)"
    if [ "${CI_COMMIT_BRANCH}" = "${MainBranch}" ]; then
        Context="main-branch"
    else
        Context="non-main-branch"
        git checkout $CI_COMMIT_BRANCH
    fi
elif [ "${CI_COMMIT_TAG}" != "" ]; then
    # TODO regex v0.0.0
    Context="version-tag"
    git checkout $CI_COMMIT_BRANCH
else
    echo "[ERROR] This pipeline is running in an anexpected context"
    echo "CI_COMMIT_REF_NAME: ${CI_COMMIT_SHA}"
    echo "CI_COMMIT_SHA:      ${CI_COMMIT_SHA}"
    exit 1
fi
echo "[INFO] Context: ${Context}"

ActualCommitSha="$(git log --pretty=format:'%H' -n 1)"
if [ "${CI_COMMIT_SHA}" != "${ActualCommitSha}" ]; then
    echo "[ERROR] This pipeline does not point to actual '${CI_COMMIT_REF_NAME}'"
    echo "CI_COMMIT_SHA:   ${CI_COMMIT_SHA}"
    echo "ActualCommitSha: ${ActualCommitSha}"
    exit 1
fi

_add_write_repository_permission () {
    if [ "${GITLAB_USER_NAME}" = "" ] \
    || [ "${GITLAB_USER_EMAIL}" = "" ] \
    || [ "${GITLAB_USER_WRITE_REPOSITORY_TOKEN}" = "" ]; then
        echo "[ERROR] Missing write repository permission"
        echo "Check that the following variables are set:"
        echo "- GITLAB_USER_NAME"
        echo "- GITLAB_USER_EMAIL"
        echo "- GITLAB_USER_WRITE_REPOSITORY_TOKEN"
        exit 1
    fi
    git config user.name "${GITLAB_USER_NAME}"
    git config user.email "${GITLAB_USER_EMAIL}"
    Url="https://${GITLAB_USER_NAME}:${GITLAB_USER_WRITE_REPOSITORY_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
    git remote set-url origin "${Url}"
}

_tag () {
    Tag="${1}"
    Message="${2}"

    # TODO check if tag exist before try deleting
    if git tag -d "${Tag}"; then
        git push --delete origin "${Tag}"
    fi
    git tag -a "${Tag}" -m "${Message}"
    git push origin "${Tag}"
}

_echo_previous_version () {
    VersionTags=$(git tag -l v*\.*\.* --sort=-version:refname)
    if [ "x${VersionTags}" = "x" ]; then
        PreviousVersion="0.0.0"
    else
        PreviousVersion=$(echo $VersionTags | cut -f1 -d" ")
        PreviousVersion=${PreviousVersion:1}
    fi
    echo "${PreviousVersion}"
}

_expected_context () {
    if [ "${1}" != "${Context}" ]; then
        echo "[ERROR] This pipeline is running in an anexpected context"
        echo "Current context:  ${Context}"
        echo "Expected context: ${1}"
        exit 1
    fi
}

tag () {
    _expected_context main-branch
    _add_write_repository_permission

    Tag="${1}"
    _tag "${Tag}" "${Tag}"
}

tag-version () {
    _expected_context main-branch
    _add_write_repository_permission

    PreviousVersion="$(_echo_previous_version)"

    ChangeLevel="${1}"
    # TODO read all commit messages after previous version to define the change level

    PreviousMajor=$(echo ${PreviousVersion} | cut -f1 -d.)
    PreviousMinor=$(echo ${PreviousVersion} | cut -f2 -d.)
    PreviousPatch=$(echo ${PreviousVersion} | cut -f3 -d.)

    NextMajor=${PreviousMajor}
    NextMinor=${PreviousMinor}
    NextPatch=${PreviousPatch}

    if [ "${ChangeLevel}" = "major" ]; then
        let "NextMajor++" || true
        NextMinor="0"
        NextPatch="0"
    elif [ "${ChangeLevel}" = "minor" ]; then
        let "NextMinor++" || true
        NextPatch="0"
    else
        let "NextPatch++" || true
    fi

    vMajor="v${NextMajor}"
    vMinor="v${NextMajor}.${NextMinor}"
    vPatch="v${NextMajor}.${NextMinor}.${NextPatch}"

    _tag ${vPatch} "Bump version: v${PreviousMajor}.${PreviousMinor}.${PreviousPatch} -> ${vPatch}"
    _tag ${vMinor} "Bump version: v${PreviousMajor}.${PreviousMinor} -> ${vMinor}"
    _tag ${vMajor} "Bump version: v${PreviousMajor} -> ${vMajor}"
}

$@
