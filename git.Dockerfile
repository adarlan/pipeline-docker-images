FROM alpine
RUN apk add git bash tree

COPY git.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/git.sh
