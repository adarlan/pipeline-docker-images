FROM hashicorp/terraform:1.5
ENTRYPOINT [ "" ]

RUN apk add bash tree

COPY terraform.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/terraform.sh
