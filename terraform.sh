#!/bin/bash
set -e

echo "_____________________________________________________________________________"
echo "[INFO] WORKING DIRECTORY"
echo "$(pwd)"
tree -a -I .git -L 1

_PROJECT_DIR=$(pwd)

_cp_backend_file () {
    [ ! -f "${TF_BACKEND}" ] && return 0
    # TODO check if variable is present, then if it is a file

    # TODO [ -f backend.tf ] && _error

    echo "_____________________________________________________________________________"
    echo "[INFO] COPYING TERRAFORM BACKEND CONFIGURATION FILE"
    echo "Source:      ${TF_BACKEND}"
    echo "Destination: backend.tf"

    cp $TF_BACKEND ./backend.tf
}

_cp_provider_files () {

    # TODO [ -f "*.backend.tf" ] && _error

    for VariableName in $(env | grep '^TF_PROVIDER_' | cut -d '=' -f 1); do
        ProviderName="${VariableName#TF_PROVIDER_}"
        Source="${!VariableName}"
        # TODO [ ! -f "${Source}" ] && _error
        Destination="${ProviderName}.provider.tf"
        echo "_____________________________________________________________________________"
        echo "[INFO] COPYING ${ProviderName^^} PROVIDER CONFIGURATION FILE"
        echo "${ProviderName}: ${Source} -> ${Destination}"

        cp $Source "./${Destination}"
    done
}

_init () {
    echo "_____________________________________________________________________________"
    echo "[INFO] TERRAFORM INIT"
    echo "Command: terraform init"

    terraform init
}

check-format () {
    echo "_____________________________________________________________________________"
    echo "[INFO] TERRAFORM CHECK FORMAT"
    echo "Command: terraform fmt -diff -check -recursive ."

    terraform fmt -diff -check -recursive .
}

validate () {
    _cp_backend_file
    _cp_provider_files
    _init

    echo "_____________________________________________________________________________"
    echo "[INFO] TERRAFORM VALIDATE"
    echo "Command: terraform validate ."

    terraform validate .
}

plan () {
    rm -f create.tfplan
    _cp_backend_file
    _cp_provider_files
    _init

    echo "_____________________________________________________________________________"
    echo "[INFO] TERRAFORM PLAN"
    echo "Command: terraform plan -input=false -out create.tfplan"

    terraform plan -input=false -out create.tfplan
}

apply () {
    _cp_backend_file
    _cp_provider_files
    _init

    echo "_____________________________________________________________________________"
    echo "[INFO] TERRAFORM APPLY"
    echo "Command: terraform apply create.tfplan"

    terraform apply create.tfplan
}

rollback () {
    if [ "$(git tag -l stable)" != "" ]; then

        echo "_____________________________________________________________________________"
        echo "[INFO] ROLLING BACK TO STABLE VERSION"

        git checkout stable
        # TODO fail if stable is the current version
        _cp_backend_file
        _cp_provider_files
        # TODO evaluate TF_VAR_* variables present in .gitlab-ci.yml
        _init

        echo "_____________________________________________________________________________"
        echo "[INFO] TERRAFORM APPLY"
        echo "Command: terraform apply -auto-approve -input=false"

        terraform apply -auto-approve -input=false
    else
        echo "_____________________________________________________________________________"
        echo "[ERROR] UNABLE TO ROLLBACK SINCE THERE IS NO STABLE VERSION"

        exit 1
    fi
}

plan-destroy () {
    rm -f destroy.tfplan
    _cp_backend_file
    _cp_provider_files
    _init

    echo "_____________________________________________________________________________"
    echo "[INFO] TERRAFORM PLAN DESTROY"
    echo "Command: terraform plan -destroy -out destroy.tfplan"

    terraform plan -destroy -out destroy.tfplan
}

apply-destroy () {
    _cp_backend_file
    _cp_provider_files
    _init

    echo "_____________________________________________________________________________"
    echo "[INFO] TERRAFORM APPLY DESTROY"
    echo "Command: terraform apply destroy.tfplan"

    terraform apply destroy.tfplan
}

${@}
