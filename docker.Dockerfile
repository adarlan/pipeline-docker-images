FROM docker:19.03
RUN apk add git bash

COPY docker.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker.sh
